import asyncio
import aiohttp

import auth


async def main():
    headers = {"Authorization": f"Bearer {auth.TOKEN}"}
    async with aiohttp.ClientSession() as session:
        async with session.get("https://lichess.org/api/stream/event", headers=headers) as response:
            print("Status:", response.status)
            print("Content-Type:", response.headers["content-type"])

            rjson = await response.json()
            print("Body:\n", rjson)


loop = asyncio.get_event_loop()
loop.run_until_complete(main())