import chess
import playsound
import tkinter


COLOR_BACKGROUND = "black"
COLOR_SQUARE_LIGHT = "white"
COLOR_SQUARE_DARK = "blue"
COLOR_LEGAL_MOVE = "grey"
LEGAL_MOVE_RADIUS = 8
SQUARE_SIZE = 64
BOARD_WIDTH = 8
BOARD_HEIGTH = 8
BORDER_WIDTH = 0
HIGHLIGHT_THICKNESS = 0


class PromotionDialog(tkinter.Toplevel):

    def __init__(self, parent, color):
        tkinter.Toplevel.__init__(self, parent)
        self.selection = chess.QUEEN

        self.queen_image = tkinter.PhotoImage(file=image_file_selection(color, chess.QUEEN))
        self.rook_image = tkinter.PhotoImage(file=image_file_selection(color, chess.ROOK))
        self.bishop_image = tkinter.PhotoImage(file=image_file_selection(color, chess.BISHOP))
        self.knight_image = tkinter.PhotoImage(file=image_file_selection(color, chess.KNIGHT))

        self.qlabel = tkinter.Button(self, image=self.queen_image, command= lambda: self.make_selection(chess.QUEEN)).pack()
        self.rlabel = tkinter.Button(self, image=self.rook_image, command= lambda: self.make_selection(chess.ROOK)).pack()
        self.blabel = tkinter.Button(self, image=self.bishop_image, command= lambda: self.make_selection(chess.BISHOP)).pack()
        self.klabel = tkinter.Button(self, image=self.knight_image, command= lambda: self.make_selection(chess.KNIGHT)).pack()

    def make_selection(self, piece_type):
        self.selection = piece_type
        self.destroy()

    def show(self):
        self.wm_deiconify()
        self.wait_window()
        return self.selection


def image_file_selection(piece_color, piece_type):
    return f"resources/images/png/{piece_color}_{chess.piece_name(piece_type)}.png"


class Piece(chess.Piece):

    def __eq__(self, other):
        return self.image == other.image

    def get_image(self):
        color_string = "white" if self.color else "black"
        self.image = tkinter.PhotoImage(file=image_file_selection(color_string, self.piece_type))
        return self.image

    @property
    def square(self):
        return self._square

    @square.setter
    def square(self, value):
        self._square = value


class BoardSquare(object):
    color_light = COLOR_SQUARE_LIGHT
    color_dark = COLOR_SQUARE_DARK

    def __init__(self, file: int, rank: int, file_for_coord: int, rank_for_coord: int, size: int):
        self.size = size
        self.square = self.calculate_square_number(file, rank)
        self.name = self.calculate_square_name(file, rank)
        self.color = self.calculate_square_color(file, rank)
        self.calculate_square_coords(file_for_coord, rank_for_coord)

    @property
    def xcenter(self):
        return self.x1 + int(self.size/2)

    @property
    def ycenter(self):
        return self.y1 + int(self.size/2)

    def calculate_square_color(self, file, rank):
        return self.color_light if (file + rank) % 2 != 0 else self.color_dark

    def calculate_square_coords(self, file, rank):
        self.x1 = file * self.size
        self.y1 = rank * self.size
        self.x2 = self.x1 + self.size
        self.y2 = self.y1 + self.size

    def calculate_square_number(self, file, rank):
        return rank * 8 + file

    def calculate_square_name(self, file, rank):
        return  f"{chess.FILE_NAMES[file]}{chess.RANK_NAMES[rank]}"


class BoardFrame(tkinter.Frame):

    def __init__(self, parent: tkinter.Tk):
        self.board = chess.Board() # Generate with FEN on new game button later probably
        self.squares = []
        self.pieces = []
        self.rows = BOARD_HEIGTH
        self.columns = BOARD_WIDTH
        self.size = SQUARE_SIZE
        self.moving_piece = None

        tkinter.Frame.__init__(self, parent)
        self.canvas = tkinter.Canvas(self, borderwidth=BORDER_WIDTH, highlightthickness=HIGHLIGHT_THICKNESS, width=self.columns*self.size, height=self.rows*self.size, background=COLOR_BACKGROUND)
        self.canvas.pack(side="top", fill="both", expand=True, padx=2, pady=2)
        self.canvas.bind("<Button-1>", self.move)
        self.canvas.bind("<B1-Motion>", self.move_if)
        self.canvas.bind("<ButtonRelease-1>", self.drop)
        self.canvas.bind("<Button-2>", self.flip)
        self.generate_squares()
        self.draw_squares()

        # TODO: Should occur on press New Game later...
        self.generate_pieces_from_fen()
        self.draw_pieces()

    def square_by_number(self, number):
        square = next((s for s in self.squares if s.square == number), None)
        return square

    def square_at_coord(self, x, y):
        for square in self.squares:
            if square.x1 < x < square.x2:
                if square.y1 < y < square.y2:
                    return square

    def piece_at_coord(self, x, y):
        square = self.square_at_coord(x, y)
        if square:
            piece = next((p for p in self.pieces if square.square == p.square), None)
            if piece:
                return piece

    def piece_at_square(self, number):
        for piece in self.pieces:
            if piece.square == number:
                return piece

    def generate_squares(self):
        file_for_coord = 0
        rank_for_coord = 0
        for file in range(8):
            for rank in range(7, -1, -1):
                self.squares.append(BoardSquare(file, rank, file_for_coord, rank_for_coord, self.size))
                rank_for_coord += 1
            rank_for_coord = 0
            file_for_coord += 1
        # Sort for flip mechanics
        self.squares.sort(key=lambda x: x.square)

    def generate_pieces_from_fen(self):
        board_fen = self.board.fen().split(" ")[0]
        file = 0
        rank = 7
        for character in board_fen:
            if character == "/":
                file = 0
                rank -= 1
            else:
                if character.isdigit():
                    file += int(character)
                else:
                    square = rank * 8 + file
                    piece = Piece.from_symbol(character)
                    piece.get_image()
                    piece.square = square
                    self.pieces.append(piece)
                    file += 1

    def draw_squares(self):
        for square in self.squares:
            self.canvas.create_rectangle(square.x1, square.y1, square.x2, square.y2, outline="black", fill=square.color, tags="square")

    def draw_pieces(self):
        for piece in self.pieces:
            square = next((s for s in self.squares if s.square == piece.square), None)
            x0 = square.x1 + int(self.size/2)
            y0 = square.y1 + int(self.size/2)
            piece.drawing = self.canvas.create_image(x0, y0, image=piece.image, tag="piece")

    def draw_legal_moves(self):
        for square in chess.SQUARES:
            if self.is_promotion(square):
                move = chess.Move(self.moving_piece.square, square, promotion=chess.QUEEN)
            else:
                move = chess.Move(self.moving_piece.square, square)
            if self.board.is_legal(move):
                boardsquare = next((bs for bs in self.squares if square == bs.square), None)
                self.canvas.create_oval(boardsquare.xcenter - LEGAL_MOVE_RADIUS, boardsquare.ycenter - LEGAL_MOVE_RADIUS, boardsquare.xcenter + LEGAL_MOVE_RADIUS, boardsquare.ycenter + LEGAL_MOVE_RADIUS, fill=COLOR_LEGAL_MOVE, tag="legalmove")

    def clear_legal_moves(self):
        self.canvas.delete("legalmove")

    def clear_moving_piece(self):
        self.canvas.delete(self.moving_piece.drawing)
        square = next((s for s in self.squares if s.square == self.moving_piece.square), None)
        self.moving_piece.drawing = self.canvas.create_image(square.xcenter, square.ycenter, image=self.moving_piece.image, tags="piece")
        self.moving_piece = None

    def flip(self, event):
        self.canvas.delete("piece")
        # TODO: bruh
        start, end, inc = (63, -1, -1) if self.squares[0].square == 0 else (0, 64, 1)
        for i, square in zip(range(start, end, inc), self.squares):
            square.square = i
        self.draw_pieces()

    def move_if(self, event):
        if self.moving_piece:
            self.move(event)

    def move(self, event):
        if not self.moving_piece:
            self.moving_piece = self.piece_at_coord(event.x, event.y)
            if self.moving_piece:
                if self.moving_piece.color == self.board.turn:
                    self.draw_legal_moves()
                else:
                    self.moving_piece = None
        if self.moving_piece:
            self.canvas.delete(self.moving_piece.drawing)
            self.moving_piece.drawing = self.canvas.create_image(event.x, event.y, image=self.moving_piece.image, tags="piece")

    def get_castling_rook(self, move):
        rooks = [p for p in self.pieces if p.color == self.moving_piece.color and p.piece_type == chess.ROOK]
        return rooks[min(range(len(rooks)), key= lambda i: abs(rooks[i].square - move.to_square))]

    def get_en_passant_pawn(self, move):
        return self.piece_at_square(move.to_square-8) if self.board.turn else self.piece_at_square(move.to_square+8)

    def get_castling_destination(self, square):
        castling_map = {
            0: (self.square_by_number(3), self.square_by_number(2)),
            7: (self.square_by_number(5), self.square_by_number(6)),
            56: (self.square_by_number(59), self.square_by_number(58)),
            63: (self.square_by_number(61), self.square_by_number(62))
        }
        return castling_map[square]

    def is_promotion(self, square):
        return self.moving_piece.piece_type == chess.PAWN and ((self.board.turn and chess.square_rank(square) == 7) or (not self.board.turn and chess.square_rank(square) == 0))

    def drop(self, event):
        if self.moving_piece:
            square = self.square_at_coord(event.x, event.y)
            if square:
                if self.is_promotion(square.square):
                    move = chess.Move(self.moving_piece.square, square.square, promotion=chess.QUEEN)
                else:
                    move = chess.Move(self.moving_piece.square, square.square)
                if self.board.is_legal(move):
                    sound = "resources/sounds/Move.mp3"
                    if self.board.is_castling(move):
                        rook = self.get_castling_rook(move)
                        rook_square, square = self.get_castling_destination(rook.square)
                        rook.square = rook_square.square
                        self.canvas.delete(rook.drawing)
                        rook.drawing = self.canvas.create_image(rook_square.xcenter, rook_square.ycenter, image=rook.image, tags="piece")
                    elif self.board.is_capture(move):
                        sound = "resources/sounds/Capture.mp3"
                        if self.board.is_en_passant(move):
                            captured_piece = self.get_en_passant_pawn(move)
                        else:
                            captured_piece = self.piece_at_coord(event.x, event.y)
                        self.canvas.delete(captured_piece.drawing)
                        self.pieces.remove(captured_piece)

                    if self.is_promotion(square.square):
                        color_string = "white" if self.moving_piece.color else "black"
                        promotion_choice = PromotionDialog(self, color_string).show()
                        move.promotion = promotion_choice
                        self.moving_piece.piece_type = promotion_choice
                        self.moving_piece.get_image()

                    self.moving_piece.square = square.square
                    self.canvas.delete(self.moving_piece.drawing)
                    self.moving_piece.drawing = self.canvas.create_image(square.xcenter, square.ycenter, image=self.moving_piece.image, tags="piece")
                    self.board.push(move)
                    playsound.playsound(sound, block=False)
                # Not legal move
                else:
                    self.clear_moving_piece()
            # Did not drop piece on square
            else:
                self.clear_moving_piece()

        self.moving_piece = None
        self.clear_legal_moves()


if __name__ == "__main__":
    root = tkinter.Tk()
    board = BoardFrame(root)
    board.pack(side="top", fill="both", expand="true", padx=2, pady=2)
    root.resizable(False, False)
    root.mainloop()